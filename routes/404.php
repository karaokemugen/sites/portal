<?php
if(!defined('BOOTSTRAP_LOADED'))
  die("Direct access is not authorized");

require(ENV_ROOTPATH.'/components/html/header.php');
require(ENV_ROOTPATH.'/components/header.php');
?>
<main>
  <blockquote class="error">
    <p>Page introuvable</p>
  </blockquote>
</main>

<?php
require(ENV_ROOTPATH.'/components/footer.php');
require(ENV_ROOTPATH.'/components/html/footer.php');