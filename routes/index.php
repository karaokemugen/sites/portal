<?php
if(!defined('BOOTSTRAP_LOADED'))
  die("Direct access is not authorized");

require(ENV_ROOTPATH.'/components/html/header.php');
require(ENV_ROOTPATH.'/components/header.php');
?>
<main>
  <div class="cards">
    <a href="https://mugen.karaokes.moe/<?=ENV_LNG=='en' ? 'en/':'';?>" class="karaoke-mugen">
      <dl>
        <dt>
          <span class="icon"><?=inline_svg(ENV_ROOTPATH.'/assets/images/karaoke-mugen.svg','5em','5em');?></span>
          <?=p__("Karaoke Mugen","Application");?>
        </dt>
        <dd><?=__("Organisez vos propres karaokés chez vous ou en convention !");?></dd>
      </dl>
    </a>
    <a href="https://kara.moe" class="karaoke-karas">
      <dl>
        <dt>
          <span class="icon"><?=inline_svg(ENV_ROOTPATH.'/assets/images/karaoke-karas.svg','5em','5em');?></span>
          <?=__("Base de Karaokés");?>
        </dt>
        <dd><?=__("Accédez à la base de données de karaokés, mise à jour quotidiennement !");?></dd>
      </dl>
    </a>
    <a href="https://map.karaokes.moe/" class="karaoke-suggest">
      <dl>
        <dt>
          <span class="icon"><?=inline_svg(ENV_ROOTPATH.'/assets/images/karaoke-suggest.svg','5em','5em');?></span>
          <?=__("Evènements");?>
        </dt>
        <dd><?=__("Trouvez les endroits dans le monde où il y a du karaoké ! Proposez vos propres évènements !");?></dd>
      </dl>
    </a>
    <a href="https://mugen.karaokes.moe/<?=ENV_LNG=='en' ? 'en/':'';?>participate.html" class="karaoke-support">
      <dl>
        <dt>
          <span class="icon"><?=inline_svg(ENV_ROOTPATH.'/assets/images/karaoke-support.svg','5em','5em');?></span>
          <?=__("Participer");?>
        </dt>
        <dd><?=__("Vous voulez contribuer au projet ? C'est par ici.");?></dd>
      </dl>
    </a>
  </div>
</main>

<?php
require(ENV_ROOTPATH.'/components/footer.php');
require(ENV_ROOTPATH.'/components/html/footer.php');
