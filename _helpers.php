<?php
function route_url($code,$language=null)
{
    $code = $code ? $code : ENV_ROUTE;
    $language = $language ? $language : ENV_LNG;
    return ENV_BASEPATH.'/'.$language.'/'.($code!='index' ? $code:'');
}

//       #     #     ##
//            ##    #  #
//      ##     #    #  #  ###
//       #     #     ##   #  #
//       #     #    #  #  #  #
//       #     #    #  #  #  #
//      ###    #     ##   #  #
//
function __locale($code=null) // convert local shortcode to full iso standard with charset
{
    switch ($code) {
        case 'fr': return 'fr_FR.UTF-8'; break;
        case 'en': return 'en_US.UTF-8'; break;
        case 'de': return 'de_DE.UTF-8'; break;
        case 'it': return 'it_IT.UTF-8'; break;
        case 'es': return 'es_ES.UTF-8'; break;
        // add more language conversion here if needed
        // ...
        default: return false; break;
    }
}
function __localeSet($code)
{
    setlocale(LC_TIME, __locale($code));

    $t = \Gettext\BaseTranslator::$current;
    $t->__currentLocal = $code;
    $file = __DIR__.'/locales/'.preg_replace('/\..+$/','',__locale($code)).'/messages.po';
    if(file_exists($file))
    {
        $translations = Gettext\Translations::fromPoFile($file);
        $t->loadTranslations($translations);
    }
}
function __localeGet()
{
    $t = \Gettext\BaseTranslator::$current;
    return $t->__currentLocal;
}

//                        ####   #          ##       #
//                   #    #                  #       #
//       ###   ##   ####  #     ##     ##    #     ###
//      #  #  #  #   #    ###    #    #  #   #    #  #
//      #  #  ####   #    #      #    ####   #    #  #
//       ###  #      #    #      #    #      #    #  #
//         #   ##     ##  #     ###    ##   ###    ###
//       ##
function getField($array,$field,$default=NULL,$notempty=false){
    if(is_array($field)){
        $v = $array;
        foreach($field as $f)
            $v = getField($v,$f,$default,$notEmpty);
        return $v;
    }
    if(is_array($array) && isset($array[$field]))
    {
        if($notempty && ($array[$field]===NULL || $array[$field]===''))
        {
            return $default;
        }
        return $array[$field];
    }
    if(is_object($array) && isset($array->$field))
    {
        if($notempty && ($array->$field===NULL || $array->$field===''))
        {
            return $default;
        }
        return $array->$field;
    }
    return $default;
}

//       #          ##     #
//                   #
//      ##    ###    #    ##    ###    ##          ###  #  #   ###
//       #    #  #   #     #    #  #  #  #        #     #  #  #  #
//       #    #  #   #     #    #  #  ####         ##   #  #  #  #
//       #    #  #   #     #    #  #  #              #   ##    ###
//      ###   #  #  ###   ###   #  #   ##         ###    ##      #
//                                         #####               ##
// injection de svg en mode inline (avec nettoyage automatique du code SVG)
function inline_svg($svgfile, $maxWidth=null, $maxHeight=null)
{
    if(file_exists($svgfile))
    {
        $svg = file_get_contents($svgfile);
        $svg = preg_replace('/(?=<!--)([\s\S]*?)-->/','',$svg);
        $svg = preg_replace('/(?=<\?xml)([\s\S]*?)\?>/','',$svg);

        // svg basics cleanup
        $svg = preg_replace("/(<svg[^>]*)[\t ]style=\"[^\"]+\"/", '\\1', $svg);
        $svg = preg_replace("/(<svg[^>]*)[\t ]x=\"[^\"]+\"/", '\\1', $svg);
        $svg = preg_replace("/(<svg[^>]*)[\t ]y=\"[^\"]+\"/", '\\1', $svg);
        $svg = preg_replace("/(<svg[^>]*)[\t ]width=\"[^\"]+\"/", '\\1', $svg);
        $svg = preg_replace("/(<svg[^>]*)[\t ]height=\"[^\"]+\"/", '\\1', $svg);
        $svg = preg_replace("/(<svg[^>]*)[\t ]class=\"[^\"]+\"/", '\\1', $svg);
        $svg = preg_replace("/(<svg[^>]*)[\t ]id=\"[^\"]+\"/", '\\1', $svg);

        // inline svg self reference classes
        // work only on direct class selector
        preg_match_all('/\.([a-z0-9_-]+)[\t ]*{([^}]*)}/i',$svg,$regs);
        //print_r($regs);
        foreach($regs[1] as $k=>$class)
        {
            $style = $regs[2][$k];
            $svg = str_replace('class="'.$class.'"','style="'.$style.'"',$svg);
        }
        $svg = preg_replace('/(?=<style)([\s\S]*?)<\/style>/','',$svg);

        if($maxWidth ||  $maxHeight) {
            $svg = str_replace('<svg ','<svg style="'.($maxWidth ? 'max-width:'.$maxWidth.';':'').($maxHeight ? 'max-height:'.$maxHeight.';':'').'"',$svg);
        }
        return $svg;
    }
    return false;
}