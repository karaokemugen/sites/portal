﻿<?php
include_once __DIR__.'/vendor/autoload.php';
include_once __DIR__.'/_helpers.php';

use Gettext\Translator;

// Définition de l'environnement à partir du context d'appel

// Eg.
// [REQUEST_URI] => /edsa-karaportal/en/test
// [SCRIPT_NAME] => /edsa-karaportal/bootstrap.php
// [PHP_SELF] => /edsa-karaportal/bootstrap.php

define('BOOTSTRAP_LOADED', true);
define('ENV_ROOTPATH', realpath(__DIR__));
define('ENV_BASEPATH', str_replace('/bootstrap.php', '', getField($_SERVER, 'SCRIPT_NAME', getField($_SERVER, 'PHP_SELF'))));
define('ENV_REQUEST', preg_replace('/\/$/','',preg_replace('/^\//','',str_replace(ENV_BASEPATH, '', getField(explode('?',getField($_SERVER, 'REQUEST_URI')), 0)))));

$env_route = null;
if(preg_match('/^[a-z]{2}(\/.*)?$/', ENV_REQUEST))
	$env_route = preg_replace('/^[a-z]{2}\/?/', '', ENV_REQUEST);
else if(ENV_REQUEST)
	$env_route = ENV_REQUEST;

define('ENV_ROUTE', $env_route ? $env_route : 'index');

// extraction de la langue courante
if (preg_match('/^([a-z]{2})(\/.*)?$/', ENV_REQUEST, $reg))
	define('ENV_LNG', $reg[1]);
else
	define('ENV_LNG', false);

// Debug ENV
if(isset($_GET['DEBUG_ENV']))
{
	echo '<pre>'.print_r(array('ENV_BASEPATH'=>ENV_BASEPATH,'ENV_REQUEST'=>ENV_REQUEST,'ENV_ROUTE'=>ENV_ROUTE,'ENV_LNG'=>ENV_LNG),1).'</pre>';
	die();
}

if(!ENV_LNG)
{
	$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	if(__locale($lang))
		header('location:'.route_url('',$lang));
	else
		header('location:'.route_url('','fr'));
	exit();
}

// Translator
$t = new Translator();
// create global function to handle translation as described bellow
$t->register();
/*
__(original)                                     →  gettext
n__(original, plural, value)                     →  ngettext:1,2
p__(context, original)                           →  pgettext:1c,2
d__(domaine, original)                           →  dgettext:2
dp__(domaine, context, original)                 →  dpgettext:2c,3
dnp__(domain, context, original, plural, value)  →  dnpgettext:2c,3,4
*/
__localeSet(ENV_LNG);

if (file_exists(__DIR__.'/routes/'.ENV_ROUTE.'.php'))
	require __DIR__.'/routes/'.ENV_ROUTE.'.php';
else
	require __DIR__.'/routes/404.php';
?>