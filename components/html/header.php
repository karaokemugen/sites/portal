<?php
if(!defined('BOOTSTRAP_LOADED'))
  die("Direct access is not authorized");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Karaoke Mugen - <?=__("Le Karaoke Infini");?></title>
  <link rel="stylesheet" href="<?=ENV_BASEPATH;?>/assets/style.css">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body>
