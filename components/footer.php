<?php
if(!defined('BOOTSTRAP_LOADED'))
  die("Direct access is not authorized");
?>
<footer>
  <ul class="footer-bar">
    <li class="logo"><?=__("Logo par");?> <a href="https://sedeto.fr">Sedeto</a></li>
    <li class="licence"><a href=https://opensource.org/licenses/mit-license.php><?=__("License MIT");?></a></li>
    <li class="git"><a href="https://gitlab.com/karaokemugen/sites/portal"><?=__("Dépot Git");?></a></li>
    <li class="language">
    	<a href="<?=route_url(null,'fr')?>">FR</a> | <a href="<?=route_url(null,'en')?>">EN</a>
    </li>
    <li class="icons"><?=__("Icônes par");?> <a href="https://www.flaticon.com/authors/freepik">Freepik</a>.</li>
  </ul>
  <p class="credits"><?=__("Les images, musiques et textes utilisés sur ce site appartiennent à leurs auteurs respectifs.");?></p>
</footer>